Array size = 268435456 (elements), Offset = 0 (elements)
Number of Threads counted = 1
-------------------------------------------------------------
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:            7128.6     0.603957     0.602501     0.605854
Scale:           4073.7     1.057685     1.054311     1.065329
Add:             3077.6     2.095245     2.093317     2.096854
Triad:           2730.7     2.362526     2.359245     2.366025
-------------------------------------------------------------
Solution Validates: avg error less than 1.000000e-13 on all three arrays

-------------------------------------------------------------
Array size = 268435456 (elements), Offset = 0 (elements)
Number of Threads counted = 20
-------------------------------------------------------------
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:           12289.4     0.385788     0.349486     0.430928
Scale:          12845.6     0.379385     0.334353     0.436329
Add:            14920.4     0.497878     0.431789     0.622043
Triad:          12849.0     0.597634     0.501396     0.789281
-------------------------------------------------------------
Solution Validates: avg error less than 1.000000e-13 on all three arrays

-------------------------------------------------------------
Array size = 268435456 (elements), Offset = 0 (elements)
Number of Threads counted = 40
-------------------------------------------------------------
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:           12293.6     0.385506     0.349365     0.430769
Scale:          13675.2     0.360084     0.314071     0.417446
Add:            14924.8     0.497580     0.431661     0.621598
Triad:          12845.5     0.597811     0.501534     0.790212
-------------------------------------------------------------
Solution Validates: avg error less than 1.000000e-13 on all three arrays

-------------------------------------------------------------
Array size = 268435456 (elements), Offset = 0 (elements)
Number of Threads counted = 80
-------------------------------------------------------------
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:           12203.8     0.382730     0.351937     0.429194
Scale:          12815.4     0.371664     0.335142     0.420500
Add:            14915.2     0.497113     0.431938     0.615650
Triad:          12828.2     0.590363     0.502211     0.779508
-------------------------------------------------------------
Solution Validates: avg error less than 1.000000e-13 on all three arrays

-------------------------------------------------------------
