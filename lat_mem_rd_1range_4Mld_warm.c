/*
 * lat_mem_rd.c - measure memory load latency
 *
 * usage: lat_mem_rd size-in-MB stride [stride ...]
 *
 * Copyright (c) 1994 Larry McVoy.  Distributed under the FSF GPL with
 * additional restriction that results may published only if
 * (1) the benchmark is unmodified, and
 * (2) the version in the sccsid below is included in the report.
 * Support for this development by Sun Microsystems is gratefully acknowledged.
 */
char    *id = "$Id$\n";

#include "bench.h"
//#define N       1000000 /* Don't change this */
#define N  4000000
#define STRIDE  (512/sizeof(char *))
#define MEMTRIES        1
#define LOWER   512
void    loads(char *addr, int range, int stride);
int     step(int k);

#define mambo_cyclemode() asm volatile("or 13,13,13")
#define mambo_start_warm_trace() asm volatile("or 14,14,14")
#define mambo_stop_trace() asm volatile("or 15,15,15")


#define mfspr(rn)	({unsigned long rval; \
			asm volatile("mfspr %0, %1" \
				     : "=r" (rval) : "i" (rn)); rval;})

#define freeze_counters()       ({unsigned long rval; \
                                  unsigned long rn = 779; \
                        asm volatile("mfspr %0, %1;" \
                                     : "=r" (rval) : "i" (rn)); \
                        asm volatile("oris %0, %1, %2;" \
                                     : "=r" (rval) : "0" (rval),"i" (0x8000)); \
                        asm volatile("mtspr %0, %1;" \
                                     : : "i" (rn), "r" (rval) ); })

#define unfreeze_counters()       ({unsigned long rval; \
                                    unsigned long mask; \
                                    unsigned long rn = 779; \
                        asm volatile("mfspr %0, %1;" \
                                     : "=r" (rval) : "i" (rn)); \
                        asm volatile("lis %0, %1;" \
                                     : "=r" (mask) : "i" (0x8000)); \
                        asm volatile("not %0, %1;" \
                                     : "=r" (mask) : "0" (mask)); \
                        asm volatile("and %0, %1, %2;" \
                                     : "=r" (rval) : "0" (rval),"r" (mask)); \
                        asm volatile("mtspr %0, %1;" \
                                     : : "i" (rn), "r" (rval) ); \
                                     })

#define read_mmcr0_freeze_all() ({unsigned long mmcr0_val; \
                                  unsigned long mmcr0_spr = 779; \
                                  asm volatile("mfspr %0, %1;" \
                                               : "=r" (mmcr0_val) : "i" (mmcr0_spr)); \
                                  asm volatile("oris %0, %1, %2;" \
                                               : "=r" (mmcr0_val) : "0" (mmcr0_val),"i" (0x8000)); \
                                               mmcr0_val;})

#define read_pmcs(pmcs, mmcr0_val)      ({unsigned long mask; \
				   	  signed long mmcr0_val_new; \
                          unsigned long mmcr0_spr = 779; \
                          asm volatile("mtspr %0, %1;" \
                                       : : "i" (mmcr0_spr), "r" (mmcr0_val) ); \
                          asm volatile("mfspr %0, %1" \
                                       : "=r" (pmcs[0]) : "i" (771)); \
                          asm volatile("mfspr %0, %1" \
                                       : "=r" (pmcs[1]) : "i" (772)); \
                          asm volatile("mfspr %0, %1" \
                                       : "=r" (pmcs[2]) : "i" (773)); \
                          asm volatile("mfspr %0, %1" \
                                       : "=r" (pmcs[3]) : "i" (774)); \
                          asm volatile("mfspr %0, %1" \
                                       : "=r" (pmcs[4]) : "i" (775)); \
                          asm volatile("mfspr %0, %1" \
                                       : "=r" (pmcs[5]) : "i" (776)); \
                          asm volatile("lis %0, %1;" \
                                       : "=r" (mask) : "i" (0x8000)); \
                          asm volatile("not %0, %1;" \
                                       : "=r" (mask) : "0" (mask)); \
                          asm volatile("and %0, %1, %2;" \
                                       : "=r" (mmcr0_val_new) : "r" (mmcr0_val),"r" (mask)); \
                          asm volatile("mtspr %0, %1;" \
                                       : : "i" (mmcr0_spr), "r" (mmcr0_val_new) ); \
                                     })




int
main(int ac, char **av)
{
        int     len;
        int     range;
        int     stride;
        int     i;
        char   *addr;

        len = atoi(av[1]) * 1024; //  in KB * 1024;
/*        addr = (char *)valloc(len);
*/
        addr = (char *)memalign(4096,len);


        if (av[2] == 0) {
             	printf( "#stride=%d\n", STRIDE);
       		printf("size      PMC 1         PMC 2         PMC 3         PMC 4         PMC 5         PMC 6\n");
       		printf("======  ============  ============  ============  ============  ============  ============\n");

                //for (range = LOWER; range <= len; range = step(range)) {
		//for (range = 0; range <10; range += 1){
                        //loads(addr, range, STRIDE);
			loads(addr, len, STRIDE);
                //}
        } else {
                printf( "#stride=%d\n", STRIDE);
                printf("size      PMC 1         PMC 2         PMC 3         PMC 4         PMC 5         PMC 6\n");
                printf("======  ============  ============  ============  ============  ============  ============\n");

               for (i = 2; i < ac; ++i) {
                        stride = bytes(av[i]);
                        printf( "#stride=%d\n", stride);
                        //for (range = LOWER; range <= len; range = step(range)) {
			//for (range = 0; range <10; range += 1){
                                //loads(addr, range, stride);
				loads(addr, len, stride);

                        //}
                        printf( "\n");
                }
        }
        return(0);
}

void
loads(char *addr, int range, int stride) 	//range is size in KB
{
        register char **p = 0 /* lint */;
        int     i;
        int     tries = 0;
	unsigned long pmc1[8],pmc2[8],pmc3[8], mmcr0_val_with_freeze;

        if (stride & (sizeof(char *) - 1)) {
                printf("lat_mem_rd: stride must be aligned.\n");
                return;
        }

        if (range < stride) {
                return;
        }

        /*
         * First create a list of pointers.
         *
         * This used to go forwards, we want to go backwards to try and defeat
         * HP's fetch ahead.
         *
         * We really need to do a random pattern once we are doing one hit per
         * page.
         */
        for (i = range - stride; i >= 0; i -= stride) {
                char    *next;

                p = (char **)&addr[i];
                if (i < stride) {
                        next = &addr[range - stride];
                } else {
                        next = &addr[i - stride];
                }
                *p = next;
        }

                /* time loop with loads */
#define ONE     p = (char **)*p;
#define FIVE    ONE ONE ONE ONE ONE
#define TEN     FIVE FIVE 
#define FIFTY   TEN TEN TEN TEN TEN 
#define HUNDRED FIFTY FIFTY
#define THOUSAND HUNDRED HUNDRED HUNDRED HUNDRED HUNDRED HUNDRED HUNDRED HUNDRED HUNDRED HUNDRED
        //i = 2*N ;
	/*
	if (len > 8192)
		i = 0;	//no warm-up needed for the memory range
	else if (len > 512)
	//i = 131072;	//2x 8MB for the L3 range
	i = 262144;//131072;	//2x 8MB for the L3 range
	else if (len > 32)
		i = 8192;	//2x 512KB for the L2 range
	else			
		i = 512;	//2x 32KB for the L1 range
	*/

       /* if (range > 16777216)
                i = 0;  //no warm-up needed for the memory range
        else
       */
                i = range / stride; //read the whole array once so it gets cached

	
	mambo_cyclemode();
        while (i > 0) {
        	HUNDRED 
                i -= 100;
        }
        use_pointer((void *)p);


        /*
         * Now walk them and time it.
         */
        mmcr0_val_with_freeze = read_mmcr0_freeze_all();
	//mambo_start_warm_trace();
/*	freeze_counters();
	pmc1[0]=mfspr(771);
	pmc1[1]=mfspr(772);
	pmc1[2]=mfspr(773);
	pmc1[3]=mfspr(774);
	pmc1[4]=mfspr(775);
	pmc1[5]=mfspr(776);
	unfreeze_counters();
*/
	read_pmcs(pmc1, mmcr0_val_with_freeze);
	mambo_start_warm_trace();
        //for (tries = 0; tries < MEMTRIES; ++tries) {
 
              /* time loop with loads */
                i = N;
                //start(0);  //not using the timebase on awan
                while (i > 0) {
                        THOUSAND
                        THOUSAND
                        THOUSAND
                        THOUSAND
                        THOUSAND
                        THOUSAND
                        THOUSAND
                        THOUSAND
                        THOUSAND
                        THOUSAND
                        i -= 10000;
                }
                //i = stop(0,0);
                //use_pointer((void *)p);    //moving this to after we read the counters
                /*if (i < result) {
                        result = i;
                }
		*/
        //}
/*	freeze_counters();
        pmc2[0]=mfspr(771);
        pmc2[1]=mfspr(772);
        pmc2[2]=mfspr(773);
        pmc2[3]=mfspr(774);
        pmc2[4]=mfspr(775);
        pmc2[5]=mfspr(776);
	unfreeze_counters();
*/
        mambo_stop_trace();
	read_pmcs(pmc2, mmcr0_val_with_freeze);
	use_pointer((void *)p);

        /*
         * We want to get to nanoseconds / load.  We don't want to
         * lose any precision in the process.  What we have is the
         * milliseconds it took to do N loads, where N is 1 million,
         * and we expect that each load took between 10 and 2000
         * nanoseconds.
         *
         * We want just the memory latency time, not including the
         * time to execute the load instruction.  We allow one clock
         * for the instruction itself.  So we need to subtract off
         * N * clk nanoseconds.
         *
         * lmbench 2.0 - do the subtration later, in the summary.
         * Doing it here was problematic.
         *
         * XXX - we do not account for loop overhead here.
         */
        //result *= 1000;                 /* convert to nanoseconds */
        //fresult = result/(float) N;
        //result /= N;                            /* nanosecs per load */
        /* printf( "%.5f %d\n", range / (1024. * 1024), result); */

	//printf("[,");
        if (range >= 1048576)
		printf( "%5.2fM ", range / (1024. * 1024.));
        else if (range >= 1024)
	        printf( "%5.fK ", range / 1024.);
	else    printf( "%5.f  ", (float) range );
        //printf( ", %d ns", result);

	pmc3[0] = pmc2[0]-pmc1[0];
	pmc3[1] = pmc2[1]-pmc1[1];
	pmc3[2] = pmc2[2]-pmc1[2];
	pmc3[3] = pmc2[3]-pmc1[3];
	pmc3[4] = pmc2[4]-pmc1[4];
	pmc3[5] = pmc2[5]-pmc1[5];
	printf(" %12lu ",pmc3[0]);
	printf(" %12lu ",pmc3[1]);
	printf(" %12lu ",pmc3[2]);
	printf(" %12lu ",pmc3[3]);
	printf(" %12lu ",pmc3[4]);
	printf(" %12lu\n",pmc3[5]);

/*        printf("        %12lu ",pmc1[0]);
        printf(" %12lu ",pmc1[1]);
        printf(" %12lu ",pmc1[2]);
        printf(" %12lu ",pmc1[3]);
        printf(" %12lu ",pmc1[4]);
        printf(" %12lu\n",pmc1[5]);

        printf("        %12lu ",pmc2[0]);
        printf(" %12lu ",pmc2[1]);
        printf(" %12lu ",pmc2[2]);
        printf(" %12lu ",pmc2[3]);
        printf(" %12lu ",pmc2[4]);
        printf(" %12lu\n",pmc2[5]);
*/

}

int
step(int k)
{
        if (k < 1024) {
                k = k * 2;
        } else if (k < 4*1024) {
                k += 1024;
        } else if (k < 32*1024) {
                k += 2048;
        } else if (k < 64*1024) {
                k += 4096;
        } else if (k < 128*1024) {
                k += 8192;
        } else if (k < 256*1024) {
                k += 16384;
        } else if (k < 512*1024) {
                k += 32*1024;
        } else if (k < 4<<20) {
                k += 512 * 1024;
        } else if (k < 8<<20) {
		k += 512 * 1024;
                //k += 1<<20;
        } else if (k < 20<<20) {
		k += 512 * 1024;
                //k += 2<<20;
        } else {
		k += 512 * 1024;
                //k += 10<<20;
        }
        return (k);
}
